jQuery(document).ready(function($) {

	$(".btn-aceptar-reservar").click(function(){
		$("#reservaModal").modal('hide');
		$("#confirmadoModal").modal('show');
		window.setTimeout(
			function(){
				window.location = 'reserva-generada.html';
			}, 3000
		);
	});
			 
	$('.input-daterange').datepicker({
		weekStart: 1,
		orientation: "bottom auto",
		month: 2,
		format: "dd/mm/yyyy"
	}).on('changeDate', function (e) {
		var start = $("#datepicker_start").val();
		var end = $("#datepicker_end").val();
		var aFecha1 = start.split('/');
 		var aFecha2 = end.split('/');
		var fFecha1 = Date.UTC(aFecha1[2],aFecha1[1]-1,aFecha1[0]);
		var fFecha2 = Date.UTC(aFecha2[2],aFecha2[1]-1,aFecha2[0]);
		var dif = fFecha2 - fFecha1;
		var dias = Math.floor(dif / (1000 * 60 * 60 * 24));
		// console.log(dias);
		$("#nro_noches").val(dias);
	});

	
	$('.kv-svg').rating({
		theme: 'krajee-svg',
		filledStar: '<span class="krajee-icon krajee-icon-star"></span>',
		emptyStar: '<span class="krajee-icon krajee-icon-star"></span>',
		min: 0, max: 5, step: 0.5, size: "xs", stars: "5"
	});

	$('#dp_start').val('');
	$('#dp_end').val('');

});

jQuery('<div class="quantity-nav"><div class="quantity-button quantity-up"><img src="svg/arrow-up.svg" width="8" height="8"></div><div class="quantity-button quantity-down"><img src="svg/arrow-down.svg" width="8" height="8"></div></div>').insertAfter('.quantity input');
jQuery('.quantity').each(function() {
  var spinner = jQuery(this),
	input = spinner.find('input[type="number"]'),
	btnUp = spinner.find('.quantity-up'),
	btnDown = spinner.find('.quantity-down'),
	min = input.attr('min'),
	max = input.attr('max');

  btnUp.click(function() {
	var oldValue = parseFloat(input.val());
	if (oldValue >= max) {
	  var newVal = oldValue;
	} else {
	  var newVal = oldValue + 1;
	}
	spinner.find("input").val(newVal);
	spinner.find("input").trigger("change");
  });

  btnDown.click(function() {
	var oldValue = parseFloat(input.val());
	if (oldValue <= min) {
	  var newVal = oldValue;
	} else {
	  var newVal = oldValue - 1;
	}
	spinner.find("input").val(newVal);
	spinner.find("input").trigger("change");
  });

});


jQuery('<div class="quantity-nav"><div class="quantity-button quantity-up"><img src="svg/arrow-up.svg" width="8" height="8"></div><div class="quantity-button quantity-down"><img src="svg/arrow-down.svg" width="8" height="8"></div></div>').insertAfter('.quantityy input');
jQuery('.quantityy').each(function() {
  var spinner = jQuery(this),
	input = spinner.find('input[type="number"]'),
	btnUp = spinner.find('.quantity-up'),
	btnDown = spinner.find('.quantity-down'),
	min = input.attr('min'),
	max = input.attr('max');

  btnUp.click(function() {
	var oldValue = parseFloat(input.val());
	if (oldValue >= max) {
	  var newVal = oldValue;
	} else {
	  var newVal = oldValue + 1;
	}
	spinner.find("input").val(newVal);
	spinner.find("input").trigger("change");

	var hab = ".h_0"+newVal;
	$(hab).addClass('d-block');
	
  });

  btnDown.click(function() {
	var oldValue = parseFloat(input.val());
	if (oldValue <= min) {
	  var newVal = oldValue;
	} else {
	  var newVal = oldValue - 1;
	}
	spinner.find("input").val(newVal);
	spinner.find("input").trigger("change");

	var hab = ".h_0"+ parseInt(newVal + 1);
	$(hab).removeClass('d-block');
  });

});