jQuery(document).ready(function($) {
	// * * * * * * * * * * * * * * * * * * * * * * * * * * * * *
	// INPUT AUTOCOMPLETE
	// * * * * * * * * * * * * * * * * * * * * * * * * * * * * *
	var typeaheadSource = [
		{ id: 1, name: 'Lima' }, 
		{ id: 2, name: 'Callao' }, 
		{ id: 3, name: 'Trujillo' },
		{ id: 4, name: 'Chiclayo' },
		{ id: 5, name: 'Arequipa' },
		{ id: 6, name: 'Tumbes' },
		{ id: 7, name: 'Tarapoto' },
		{ id: 8, name: 'Piura' },
		{ id: 9, name: 'Huacho' },
		{ id: 10, name: 'Tacna' },
		{ id: 11, name: 'Puno' },
		{ id: 12, name: 'Moquegua' }
	];

	$('input.typeahead').typeahead({
		source: typeaheadSource
	});

	// * * * * * * * * * * * * * * * * * * * * * * * * * * * * *
	// INPUT CHECKBOX
	// * * * * * * * * * * * * * * * * * * * * * * * * * * * * *
	$(".img-check").click(function(){
		$(this).toggleClass("check");
	});

	// * * * * * * * * * * * * * * * * * * * * * * * * * * * * *
	// BOTONES DE MODAL
	// * * * * * * * * * * * * * * * * * * * * * * * * * * * * *
	$(".btn-aceptar-reservar").click(function(){
		$("#reservaModal").modal('hide');
		$("#confirmadoModal").modal('show');
		window.setTimeout(
			function(){
				window.location = 'reserva-generada.html';
			}, 3000
		);
	});

	// * * * * * * * * * * * * * * * * * * * * * * * * * * * * *
	// CALENDARIO
	// * * * * * * * * * * * * * * * * * * * * * * * * * * * * *
	var userLanguage = navigator.language || navigator.userLanguage;
	if (userLanguage == "en") userLanguage = "fr";
	$("#start_end").caleran({
		locale: userLanguage,
		minDate: moment(),
		autoCloseOnSelect: true,
		startOnMonday: true,
		showOn: "bottom",
		ranges: [
			{
				title: "Hoy",
				startDate: moment(),
				endDate: moment().add(1, 'days')
			},
		],
		onbeforeselect: function (caleran, startDate, endDate) {
			$("#dp_start").val(startDate.format('MM/DD/YYYY'));
			$("#dp_end").val(endDate.format('MM/DD/YYYY'));
			return true; // false prevents update
		},
		onfirstselect: function (caleran, startDate, endDate) {
			caleran.config.endDate = startDate.clone().add(1, "days");
		},
		onafterselect: function (caleran, startDate, endDate) {
			var start = startDate.format('DD/MM/YYYY');
			var end = endDate.format('DD/MM/YYYY');
			var aFecha1 = start.split('/');
			var aFecha2 = end.split('/');
			// --------------
			var fFecha1 = Date.UTC(aFecha1[2], aFecha1[1] - 1, aFecha1[0]);
			var fFecha2 = Date.UTC(aFecha2[2], aFecha2[1] - 1, aFecha2[0]);
			var diff = fFecha2 - fFecha1;
			var dias = Math.floor(diff / (1000 * 60 * 60 * 24));
			$("#nro_noches").text(dias);
		}
	});

	$('.rt').rating();

	$('#dp_start').val('');
	$('#dp_end').val('');

});

// * * * * * * * * * * * * * * * * * * * * * * * * * * * * *
// INPUT TYPE NUMBER
// * * * * * * * * * * * * * * * * * * * * * * * * * * * * *
jQuery('<div class="quantity-nav"><div class="quantity-button quantity-up"><img src="svg/arrow-up.svg" width="8" height="8"></div><div class="quantity-button quantity-down"><img src="svg/arrow-down.svg" width="8" height="8"></div></div>').insertAfter('.quantity input');
jQuery('.quantity').each(function() {
  var spinner = jQuery(this),
	input = spinner.find('input[type="number"]'),
	btnUp = spinner.find('.quantity-up'),
	btnDown = spinner.find('.quantity-down'),
	min = input.attr('min'),
	max = input.attr('max');

  btnUp.click(function() {
	var oldValue = parseFloat(input.val());
	if (oldValue >= max) {
	  var newVal = oldValue;
	} else {
	  var newVal = oldValue + 1;
	}
	spinner.find("input").val(newVal);
	spinner.find("input").trigger("change");
  });

  btnDown.click(function() {
	var oldValue = parseFloat(input.val());
	if (oldValue <= min) {
	  var newVal = oldValue;
	} else {
	  var newVal = oldValue - 1;
	}
	spinner.find("input").val(newVal);
	spinner.find("input").trigger("change");
  });

});


jQuery('<div class="quantity-nav"><div class="quantity-button quantity-up"><img src="svg/arrow-up.svg" width="8" height="8"></div><div class="quantity-button quantity-down"><img src="svg/arrow-down.svg" width="8" height="8"></div></div>').insertAfter('.quantityy input');
jQuery('.quantityy').each(function() {
  var spinner = jQuery(this),
	input = spinner.find('input[type="number"]'),
	btnUp = spinner.find('.quantity-up'),
	btnDown = spinner.find('.quantity-down'),
	min = input.attr('min'),
	max = input.attr('max');

  btnUp.click(function() {
	var oldValue = parseFloat(input.val());
	if (oldValue >= max) {
	  var newVal = oldValue;
	} else {
	  var newVal = oldValue + 1;
	}
	spinner.find("input").val(newVal);
	spinner.find("input").trigger("change");

	var hab = ".h_0"+newVal;
	$(hab).addClass('d-block');
	
  });

  btnDown.click(function() {
	var oldValue = parseFloat(input.val());
	if (oldValue <= min) {
	  var newVal = oldValue;
	} else {
	  var newVal = oldValue - 1;
	}
	spinner.find("input").val(newVal);
	spinner.find("input").trigger("change");

	var hab = ".h_0"+ parseInt(newVal + 1);
	$(hab).removeClass('d-block');
  });

});

// * * * * * * * * * * * * * * * * * * * * * * * * * * * * *
// SLIDER GALERIA
// * * * * * * * * * * * * * * * * * * * * * * * * * * * * *	


var swiper = new Swiper('.swiper-container', {
	slidesPerView: 4,
	spaceBetween: 7,
	// init: false,
	pagination: {
		el: '.swiper-pagination',
		clickable: true,
	},
	navigation: {
		nextEl: '.swiper-button-next',
		prevEl: '.swiper-button-prev',
	},
	breakpoints: {
		1024: {
			slidesPerView: 4,
			spaceBetween: 40,
		},
		768: {
			slidesPerView: 4,
			spaceBetween: 30,
		},
		640: {
			slidesPerView: 4,
			spaceBetween: 20,
		},
		320: {
			slidesPerView: 4,
			spaceBetween: 10,
		}
	}
});